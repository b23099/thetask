
#About the project

Homegate api used for displaying:
- ad list
- ad item details

#Heroku Instance

The heroku instace of the app is located 
[Here](https://rentatzurich.herokuapp.com/)

#Features
create react app used for fast pwa seed project initialization,
material-ui for fast, declarative ui prototyping
react-router for dynamic routing,
redux for state management


#Installing
run
```
yarn
```
or
```
npm install
```

proposed next features on list:
- add real api key for google maps(or find an alternative)
- pagination for the ad list endpoint,
- filters, for more granular results
- edge/proxy towards the homegate api
- users -  for notifications, favorites, search history  etc.


#Troubleshooting
If you run the app localy and get errors with incompatible npm/node versions,
just remove the engines property from package.json (the property is used for heroku configuration)

#Author
Boris Cupac
