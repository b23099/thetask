import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router';

const styles = {
	root: {
		flexGrow: 1,
	},
	flex: {
		flexGrow: 1,
	},
	menuButton: {
		marginLeft: -12,
		marginRight: 20,
	},
	AppBar: {
		backgroundColor: '#252830 !important',
	},
};

function ButtonAppBar(props) {
	const { classes, history } = props;
	return (
		<div className={classes.root}>
			<AppBar className={classes.AppBar} position="sticky">
				<Toolbar>
					<Typography variant="title" color="inherit" className={classes.flex}>
						Rent@Zurich
					</Typography>
					<Button color="inherit" onClick={() => history.push('/')}>
						Home
					</Button>
				</Toolbar>
			</AppBar>
		</div>
	);
}

ButtonAppBar.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(ButtonAppBar));
