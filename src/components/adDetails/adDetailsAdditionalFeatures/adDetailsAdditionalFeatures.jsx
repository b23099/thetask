import React, { Fragment } from 'react';
import Divider from '@material-ui/core/Divider/Divider';
import uniqueId from 'lodash.uniqueid';

export default ({ adDetails }) => {
	return (
		<Fragment>
			<p className="AdditionalFeatures">
				<span>Additional Features:</span>&nbsp;{adDetails.equipment.map(
					({ value, label }) =>
						value ? (
							<span key={uniqueId()}>
								<span>{label} &nbsp;</span>
							</span>
						) : (
							<span />
						)
				)}
			</p>
			<Divider light />
		</Fragment>
	);
};
