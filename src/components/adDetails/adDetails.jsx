import { bindActionCreators } from 'redux';
import { getAdDetails } from '../../Redux/actions/adActions';
import connect from 'react-redux/es/connect/connect';

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import uniqueId from 'lodash.uniqueid';

import Grid from '@material-ui/core/Grid';
import AdSelectedFeaturesList from './adDetailsPropertyFeatures/adDetailsPropertyFeatures';
import Carousel from 'nuka-carousel';
import AdSelectedMap from './adDetailsMap/adDetailsMap';

import { withRouter } from 'react-router';

import AdSelectedTopInfo from './adDetailsTopInfo/adDetailsTopInfo';
import PrintIcon from '@material-ui/icons/Print';

import styles from './styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Button from '@material-ui/core/Button/Button';
import AgencyDetails from './AgencyDetails/AgencyDetails';
import AdditionalFeatures from './adDetailsAdditionalFeatures/adDetailsAdditionalFeatures';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';

class AdDetails extends React.Component {
	componentDidMount() {
		this.props.getAdDetails(this.props.match.params.id);
	}

	componentDidUpdate() {
		/**
		 * Nuka carousell bug fix with initial Height being set to 0 for url loaded images.
		 */
		setTimeout(() => {
			window.dispatchEvent(new Event('resize'));
		}, 0);
	}

	render() {
		const { classes, adDetails, history, isLoading } = this.props;

		return (
			<Grid className={classes.adDetailsGrid} container direction="row" justify="center">
				{isLoading && <LoadingIndicator />}
				{adDetails && (
					<Card className={classes.card}>
						<CardHeader
							title={adDetails.title}
							subheader={adDetails.availableFromStr}
							action={
								<Button onClick={() => history.push('/list')} fullWidth size="small" color="primary">
									<ArrowBackIcon />
									Back
								</Button>
							}
						/>

						<CardContent className={classes.carouselContainer}>
							<Carousel renderBottomCenterControls={() => {}} wrapAround>
								{adDetails.realEstatePictures &&
									adDetails.realEstatePictures.map(picture => (
										<img alt="ad" key={uniqueId()} className={classes.Picture} src={picture.url} />
									))}
							</Carousel>
						</CardContent>

						<CardContent className={classes.topInfo}>
							<AdSelectedTopInfo item={adDetails} />
						</CardContent>

						<Grid
							className={classes.mapAndContent}
							container
							direction="row"
							justify="center"
							alignItems="flex-start"
						>
							<Grid item xs={12} sm={6}>
								<AdSelectedFeaturesList adDetails={adDetails} />
							</Grid>
							<Grid item xs={12} sm={6}>
								<AdSelectedMap isMarkerShown geoLocation={adDetails.geoLocation} />
							</Grid>
						</Grid>

						<CardContent>
							<AdditionalFeatures adDetails={adDetails} />
						</CardContent>
						<CardContent>
							<AgencyDetails adDetails={adDetails} />
						</CardContent>

						<CardContent>
							<Typography variant="subheading">Description:</Typography>

							<p dangerouslySetInnerHTML={(() => ({ __html: adDetails.adDescription }))()} />
						</CardContent>

						<CardContent className={classes.generalInfo}>
							<a
								disabled={!adDetails.documentUrl}
								href={adDetails.documentUrl}
								target="_blank"
								download="document.pdf"
							>
								<IconButton disabled={!adDetails.documentUrl} aria-label="DownloadPdf">
									<PrintIcon />
								</IconButton>
							</a>
						</CardContent>
					</Card>
				)}
			</Grid>
		);
	}
}

AdDetails.propTypes = {
	classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	adDetails: state.adDetails.adDetails,
	isLoading: state.adDetails.isLoading,
});

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			getAdDetails,
		},
		dispatch
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withStyles(styles)(AdDetails)));
