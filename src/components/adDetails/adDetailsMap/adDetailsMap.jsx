import React from 'react';
import { compose, withProps } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
//
const adDetailsMap = compose(
	withProps({
		googleMapURL:
			'https://maps.googleapis.com/maps/api/js?key=AIzaSyCmy2jd9CaAYRTADAqU2PQ05mmAiQCpIV8&v=3.exp&libraries=geometry,drawing,places',
		loadingElement: <div style={{ height: `100%` }} />,
		containerElement: <div style={{ height: `400px` }} />,
		mapElement: <div style={{ height: `100%` }} />,
	}),
	withScriptjs,
	withGoogleMap
)(({ geoLocation, isMarkerShown }) => {
	const coordinateSet = geoLocation.split(',');
	const [lng, lat] = coordinateSet.map(coord => parseFloat(coord));

	return (
		<section>
			<GoogleMap defaultZoom={13} defaultCenter={{ lat, lng }}>
				{isMarkerShown && <Marker position={{ lat, lng }} />}
			</GoogleMap>
		</section>
	);
});

export default adDetailsMap;
