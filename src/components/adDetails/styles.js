import red from '@material-ui/core/colors/red';

export default theme => ({
	card: {
		maxWidth: 920,
	},
	Picture:{
		objectFit: "contain",
		width: "100%",
		height:"35em"
	},
	FlexStart:{
		alignSelf:"flex-start"
	},
	infoLabel: {
		fontSize: '.85em',
		padding:".25em",
		whiteSpace:"nowrap"
	},
	NetAndAddedPrice: {
		color: '#000',
		padding:".25em"
	},
	InfoDetailsContainer: {
		alignItems: 'center',
		display: 'flex',
		flexDirection: 'column',
	},
	infoMain: {
		margin: 0,
		fontSize: "1.2em"
	},
	media: {
		height: 0,
		paddingTop: '56.25%', // 16:9
	},
	actions: {
		display: 'flex',
	},
	expand: {
		transform: 'rotate(0deg)',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.shortest,
		}),
		marginLeft: 'auto',
		[theme.breakpoints.up('sm')]: {
			marginRight: -8,
		},
	},
	expandOpen: {
		transform: 'rotate(180deg)',
	},
	avatar: {
		backgroundColor: red[500],
	},
	adDetailsGrid: {
		padding: '1em',
	},
	carouselContainer: {
		width: '100%',
		height: '37em',

	},
	topInfo: {
		'justify-content': 'space-between',
		display: 'flex',
		background: '#EEEEEE',
		justifyContent: 'center',
	},
	generalInfo: {
		justifyContent: 'flex-end',
		display: 'flex',
		backgroundColor: '#00b0ff',
	},
	mapAndContent: {
		background: '#eee',
	},
});
