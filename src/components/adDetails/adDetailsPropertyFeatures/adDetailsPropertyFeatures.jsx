import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import uniqueId from 'lodash.uniqueid';
import valueOrNA from '../../../util/valueOrNA';

const styles = theme => ({
	root: {
		width: '100%',
	},
	adDetailsTitle: {
		'text-align': 'center',
	},
});

const generateListItems = listItems => {
	return listItems.map(({ title, value }) => (
		<ListItem key={uniqueId()} style={{ textAlign: 'left', flex: 1 }} divider>
			<ListItemText style={{ padding: 0, flex: 1 }} primary={title} />
			<ListItemText style={{ textAlign: 'center', padding: 0, flex: 1 }} primary={valueOrNA(value)} />
		</ListItem>
	));
};

const constructListItems = adDetails => {
	return [
		{
			title: 'Type',
			value: adDetails.objectTypeLabel,
		},
		{
			title: 'Rooms',
			value: adDetails.numberRooms,
		},
		{
			title: 'Floors',
			value: adDetails.floor,
		},
		{
			title: 'Street',
			value: adDetails.propertyStreet,
		},
		{
			title: 'City',
			value: adDetails.propertyCityname,
		},
	];
};

function ListDividers(props) {
	const { classes, adDetails } = props;
	const listItems = adDetails ? constructListItems(adDetails) : [];

	return (
		<Fragment>
			<div className={classes.root}>
				<List component="nav">
					<ListItem>
						<Typography className={classes.adDetailsTitle} gutterBottom variant="headline">
							Property Features
						</Typography>
					</ListItem>
					{generateListItems(listItems)}
				</List>
			</div>
		</Fragment>
	);
}

ListDividers.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDividers);
