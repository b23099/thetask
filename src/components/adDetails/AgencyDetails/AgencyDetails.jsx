import React, { Fragment } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { withStyles } from '@material-ui/core';
import styles from './styles';
import Divider from '@material-ui/core/Divider/Divider';
import classNames from 'classnames';

const AgencyDetails = ({ adDetails, classes }) => {
	return (
		<Fragment>
			<List className={classNames(classes.AgencyDetailsContainer,"AgencyDetailsContainer")}>
				<ListItem className={classes.AgencyDetailsItem}>
					<ListItemText secondary="Agency Name" primary={`${adDetails.agencyName}`} />
				</ListItem>

				<ListItem className={classes.AgencyDetailsItem}>
					<ListItemText secondary="Agency Address" primary={`${adDetails.agencyStreet}`} />
				</ListItem>

				<ListItem className={classes.AgencyDetailsItem}>
					<ListItemText secondary="Agency Email" primary={`${adDetails.agencyEmail}`} />
				</ListItem>
			</List>
			<Divider light  />
		</Fragment>
	);
};

export default withStyles(styles)(AgencyDetails);
