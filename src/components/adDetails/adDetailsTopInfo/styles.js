export default theme => ({
	FlexStart: {
		alignSelf: 'flex-start',
	},
	infoLabel: {
		fontSize: '.85em',
		padding: '.25em',
		whiteSpace: 'nowrap',
	},
	NetAndAddedPrice: {
		color: '#000',
		padding: '.25em',
	},
	InfoDetailsContainer: {
		alignItems: 'center',
		display: 'flex',
		flexDirection: 'column',
		width: '10em',
	},
	infoMain: {
		margin: 0,
		fontSize: '1.2em',
	},

	adDetailsGrid: {
		padding: '1em',
	},

	generalInfo: {
		'justify-content': 'space-between',
		display: 'flex',
		backgroundColor: '#00b0ff',
	},

	topInfoList: {
		display: 'flex',
	},
});
