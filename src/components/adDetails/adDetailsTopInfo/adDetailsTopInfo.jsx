import ListItem from '@material-ui/core/ListItem';
import PriceIcon from '@material-ui/icons/AttachMoney';
import SurfaceIcon from '@material-ui/icons/FlipToFront';
import classNames from 'classnames';
import PhoneIcon from '@material-ui/icons/Phone';
import Button from '@material-ui/core/Button/Button';
import List from '@material-ui/core/List';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import valueOrNA from '../../../util/valueOrNA';

const TopInfo = ({ item, classes }) => {
	return (
		<List className={classNames(classes.topInfoList, 'TopInfoList')} disablePadding>
			<ListItem disableGutters>
				<PriceIcon />
				<div className={classes.InfoDetailsContainer}>
					<span className={classes.infoLabel}>Monthly rent</span>

					<div>
						<p className={classes.infoMain}>{`${valueOrNA(item.sellingPrice)} ${item.currency}`}</p>
					</div>

					<div className={classes.NetAndAddedPrice}>
						<span className={classes.infoLabel}>Net:{valueOrNA(item.rentNet)}</span>
						<span className={classes.infoLabel}>Extra:{valueOrNA(item.rentExtra)}</span>
					</div>
				</div>
			</ListItem>
			<ListItem disableGutters>
				<SurfaceIcon />
				<div className={classNames(classes.InfoDetailsContainer, classes.FlexStart)}>
					<span className={classes.infoLabel}>Living Space</span>

					<p className={classes.infoMain}>{`${valueOrNA(item.surfaceLiving)} m\u00B2`}</p>
				</div>
			</ListItem>

			<ListItem disableGutters className={classNames(classes.PhoneContact)}>
				<PhoneIcon />
				<Button style={{ whiteSpace: 'nowrap', width: '10rem' }} color="primary">
					<a href={`tel:${item.agencyPhoneDay}`}>{item.agencyPhoneDay || 'N/A'} </a>
				</Button>
			</ListItem>
		</List>
	);
};

export default withStyles(styles)(TopInfo);
