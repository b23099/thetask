import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import { withRouter } from 'react-router';
import styles from './styles';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
import {  getAllAds } from '../../Redux/actions/adActions';
import connect from 'react-redux/es/connect/connect';

class HomePage extends React.Component {

	componentDidMount(){
		this.props.getAllAvailableAds()
	}

	render() {
		const { classes,adsCount } = this.props;

		return (
			<Grid
				className="MenuSection__container"
				container
				direction="column"
				justify="center"
				alignItems="flex-start"
			>
				<section className="HomePageTextContainer">
					<Typography className="HomePageSubtitle" variant="display1" noWrap>
						Rent real estate in Zurich City
					</Typography>
					<Typography
						className="HomePageText HomePageTitle"
						variant="display4"
						noWrap

					>
						Rent@Zurich
					</Typography>
				</section>

				<Grid container direction="row" justify="center" alignItems="center">
					<Button
						onClick={() => this.props.history.push('/list')}
						variant="extendedFab"
						aria-label="Delete"
						className={classNames(classes.button, classes.HomePageButton)}
					>
						<HomeIcon />
						<span className={classes.AvailableProperties}>
							<span className={classes.number}>{
								adsCount
							}</span>
							<span className={classes.label}>Available Properties</span>
						</span>
					</Button>
				</Grid>
			</Grid>
		);
	}
}

HomePage.propTypes = {
	classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	adsCount: state.homePage.adsCount,
});

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			getAllAvailableAds: getAllAds,
		},
		dispatch
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withStyles(styles)(HomePage)));
