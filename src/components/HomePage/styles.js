export default theme => ({
	HomePageButton: {
		margin: '2em',
		height:"4em",
		borderRadius:"3px",
		color:"#000",
		backgroundColor: '#f0ad4e',
		borderColor: '#eea236',
		"&:hover":{
			backgroundColor: "#f3c78e",

			color: '#fff',
		}
	},


	AvailableProperties: {
		display: 'flex',
		flexDirection: 'column',
	},
	label:{
		fontSize: ".75em"
	}
});
