import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';

function LoadingIndicator(props) {
	return (
		<div>
			<CircularProgress style={{margin:"2em"}}  size={50} />
		</div>
	);
}

export default LoadingIndicator;
