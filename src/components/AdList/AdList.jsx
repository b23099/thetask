import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import AdListItem from './AdListItem/AdListItem';
import { getAdList } from '../../Redux/actions/adActions';
import { bindActionCreators } from 'redux';
import Grid from '@material-ui/core/Grid/Grid';
import styles from './styles';
import { withStyles } from '@material-ui/core';
import uniqueId from 'lodash.uniqueid';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';

class AdList extends React.Component {
	componentDidMount() {
		this.props.getAdList();
	}

	render() {
		const { adList, classes, isLoading } = this.props;
		return (
			<Fragment>
				<Grid className={classes.AdListGrid} container direction="row" justify="center" alignItems="center">
					<Grid item xs={12} className={classes.AdListContainer}>
						{isLoading && <LoadingIndicator />}

						{adList &&
							adList.items &&
							adList.items.map(item => <AdListItem key={uniqueId()} item={item} />)}
					</Grid>
				</Grid>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	adList: state.adList.adList,
	isLoading: state.adList.isLoading,
});

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			getAdList,
		},
		dispatch
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AdList));
