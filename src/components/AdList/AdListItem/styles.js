export default theme => ({
	card: {
		maxWidth: 345,
		margin:"1em",
	},
	media: {
		height: 187,
		width: 345,
	},
	content: {
		padding: 0,
	},
	root: {
		width: '100%',
		maxWidth: 360,
		backgroundColor: 'f9f9f9',
	},
	button: {
		width: '100%',
	},
	Address:{
		whiteSpace:"nowrap"
	}
});