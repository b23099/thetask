import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import SurfaceIcon from '@material-ui/icons/FlipToFront';
import AddressIcon from '@material-ui/icons/Business';
import PriceIcon from '@material-ui/icons/AttachMoney';
import PhoneIcon from '@material-ui/icons/Phone';
import { withRouter } from 'react-router';
import CardActions from '@material-ui/core/CardActions/CardActions';
import Button from '@material-ui/core/Button/Button';
import { withStyles } from '@material-ui/core';
import styles from './styles';

import valueOrNA from '../../../util/valueOrNA';

const AdListItem = ({ item, history, classes }) => {
	return (
		<Card className={classes.card}>
			<CardActionArea onClick={() => history.push(`/details/${item.advertisementId}`)}>
				<CardMedia className={classes.media} image={item.picFilename1Medium} />
				<CardContent className={classes.content}>
					<List>
						<ListItem>
							<AddressIcon />
							<ListItemText
								className={classes.Address}
								secondary="Address"
								primary={`${valueOrNA(item.city)}, ${valueOrNA(item.street)}`}
							/>
						</ListItem>
						<ListItem>
							<SurfaceIcon />
							<ListItemText
								secondary="Living Space"
								primary={`${valueOrNA(item.surfaceLiving)} m\u00B2`}
							/>
							<PriceIcon />
							<ListItemText secondary="Price" primary={`${valueOrNA(item.price)} CHF`} />
						</ListItem>

						<ListItem />
					</List>
				</CardContent>
			</CardActionArea>
			<CardActions>
				<Button fullWidth size="small" color="primary">
					<PhoneIcon />
					&nbsp; &nbsp;
					{item.contactPhone ? (
						<a href={`tel:${item.contactPhone}`}>{item.contactPhone}</a>
					) : (
						<p>No phone number available</p>
					)}
				</Button>
			</CardActions>
		</Card>
	);
};

export default withRouter(withStyles(styles)(AdListItem));
