export default theme => ({
	AdListContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		padding: '5em 0',
		justifyContent: 'center',
		backgroundColor: '#f8f9fb',
	},
	AdListGrid: {
		padding: '5em 1em',
		backgroundColor: '#F8F9FB',
	},
});
