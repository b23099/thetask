import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AdList from '../AdList/AdList';
import AppBar from '../AppBar/AppBar';
import AdSelected from '../adDetails/adDetails';
import Notifications from 'react-notification-system-redux';

import '../../Redux/actions/notificationWatcher';
import { connect } from 'react-redux';

const styles = {};

function PageContainer(props) {
	return (
		<Fragment>
			<AppBar />
			<Router>
				<Switch>
					<Route path={'/list'} exact component={AdList} />
					<Route path={'/details/:id'} exact component={AdSelected} />
				</Switch>
			</Router>

			<Notifications notifications={props.notifications} />
		</Fragment>
	);
}

const mapStateToProps = state => ({
	notifications: state.notifications,
});

export default connect(mapStateToProps)(withStyles(styles)(PageContainer));
