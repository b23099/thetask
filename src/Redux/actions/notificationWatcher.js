import actionTypes from '../constants/actionTypes';
import { error } from 'react-notification-system-redux';

import store from '../store';

const notificationWatcher = () => {
	const action = store.getState().lastAction;

	if (action.type === actionTypes.GET_AD_LIST_ERROR) {
		store.dispatch(
			error({
				message: `Failed Fetching Ad List `,
			})
		);
	} else if (action.type === actionTypes.GET_AD_DETAILS_ERROR) {
		store.dispatch(
			error({
				message: `Failed Fetching Ad Details `,
			})
		);
	}
};

store.subscribe(notificationWatcher);
