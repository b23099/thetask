import actionTypes from '../constants/actionTypes';
import axios from 'axios';

const baseUrl = 'https://www.homegate.ch/rs/real-estates';
const headers = {
	authorization: 'Basic aGdfYW5kcm9pZDo2VmNHVTZjZUNGVGs4ZEZt',
};

export const getAdList = node => dispatch => {
	dispatch(getAdListRequest());

	/**
	 * prf: price from
	 * prt: price to
	 * cou: country?
	 * nrs: items per page
	 */
	return axios
		.get(baseUrl, {
			headers,
			params: {
				loc: 'Zurich',
				lan: 'en',
				pag: 1,
				cht: 'RENTFLAT',
				nrs: 30,
				cou: 'CH',
				ver: 3,
			},
		})
		.then(response => dispatch(getAdListSuccess(response.data)), error => dispatch(getAdListError(error)));
};

export const getAllAds = () => dispatch => {
	return axios
		.get(`${baseUrl}`, {
			headers,
			params: {
				// nrs: 0,
				loc: 'Zurich',
				rfi: 'resultCount',
				lan: 'en',
				cou: 'CH',
				cht: 'RENTFLAT',
				ver: 3,
				// cli: 'android',
			},
		})
		.then(response => dispatch(getAllAdsSuccess(response.data)), error => dispatch(gettAllAdsError(error)));
};

export const getAdDetails = id => dispatch => {
	dispatch(getAdDetailsRequest());

	return axios
		.get(`${baseUrl}/${id}`, {
			headers,
			params: {
				lan: 'en',
				ver: 3,
				cli: 'android',
			},
		})
		.then(response => dispatch(getAdDetailsSuccess(response.data)), error => dispatch(getAdDetailsError(error)));
};

export const getAdDetailsRequest = () => ({
	type: actionTypes.GET_AD_DETAILS_REQUEST,
});

export const getAdDetailsSuccess = payload => ({
	type: actionTypes.GET_AD_DETAILS_SUCCESS,
	payload,
});

export const getAdDetailsError = error => ({
	type: actionTypes.GET_AD_DETAILS_ERROR,
	error,
});

export const getAllAdsSuccess = payload => ({
	type: actionTypes.GET_ALL_ADS_SUCCESS,
	payload,
});

export const gettAllAdsError = payload => ({
	type: actionTypes.GET_ALL_ADS_ERROR,
	payload,
});

export const getAdListRequest = node => ({
	type: actionTypes.GET_AD_LIST_REQUEST,
	node,
});

export const getAdListSuccess = payload => ({
	type: actionTypes.GET_AD_LIST_SUCCESS,
	payload,
});

export const getAdListError = node => ({
	type: actionTypes.GET_AD_LIST_ERROR,
	node,
});
