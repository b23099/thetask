import { createStore, applyMiddleware, compose } from 'redux';
import { combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { reducer as notifications } from 'react-notification-system-redux';

import adList from './reducers/adList';
import homePage from './reducers/homePage';
import adDetails from './reducers/adDetails';
import lastAction from './reducers/lastAction';

const store = createStore(
	combineReducers({
		adList,
		adDetails,
		homePage,
		lastAction,
		notifications,
	}),
	compose(applyMiddleware(thunkMiddleware))
);

export default store;
