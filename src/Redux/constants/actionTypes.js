import keyMirror from "keymirror";

export default keyMirror({
	GET_AD_LIST_REQUEST: null,
	GET_AD_LIST_SUCCESS:null,
	GET_AD_LIST_ERROR:null,
	GET_ALL_ADS_SUCCESS:null,
	GET_ALL_ADS_ERROR:null,
	GET_AD_DETAILS_REQUEST:null,
	GET_AD_DETAILS_SUCCESS:null,
	GET_AD_DETAILS_ERROR:null
});
