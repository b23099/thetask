import actionTypes from '../constants/actionTypes';

const initialState = {
	adList: [],
	isLoading: false,
};

const adList = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GET_AD_LIST_REQUEST:
			return {
				...state,
				adList:[],
				isLoading: true,
			};
		case actionTypes.GET_AD_LIST_SUCCESS:
			return {
				...state,
				adList: action.payload,
				isLoading: false,
			};
		case actionTypes.GET_AD_LIST_ERROR:
			return {
				...state,
				isLoading: false,
			};
		default:
			return state;
	}
};

export default adList;
