import actionTypes from '../constants/actionTypes';

const initialState = {
	adDetails: null,
	isLoading:false
};
//
const adDetails = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GET_AD_DETAILS_SUCCESS:
			return {
				...state,
				isLoading:false,
				adDetails: action.payload
			};
		case actionTypes.GET_AD_DETAILS_REQUEST:
			return {
				...state,
				adDetails:null,
				isLoading:true
			};
		default:
			return state;
	}
};

export default adDetails;