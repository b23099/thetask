import actionTypes from '../constants/actionTypes';

const initialState = {
	adsCount: 0,
};

const homePage = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GET_ALL_ADS_SUCCESS:
			return {
				...state,
				adsCount: action.payload.resultCount,
			};

		default:
			return state;
	}
};

export default homePage;
