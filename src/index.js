import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from './Redux/store';

import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PageContainer from "./components/PageContainer/PageContainer";

ReactDOM.render(
	<Provider store={store}>
		<Router>
			<Switch>
				<Route path="/" exact component={App} />
				<Route path="/list" exact component={PageContainer} />
				<Route path="/details/:id" exact component={PageContainer} />
			</Switch>
		</Router>
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();
