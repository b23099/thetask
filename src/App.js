import React, { Component } from 'react';
import './App.css';
import Homepage from './components/HomePage/HomePage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PageContainer from './components/PageContainer/PageContainer';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Homepage />

				<Router>
					<Switch>
						<Route path="/list" exact component={PageContainer} />
					</Switch>
				</Router>
			</div>
		);
	}
}

export default App;
